const { Pool } = require('pg')

// Verificamos datos de BD, y creamos pool
const connectionString = process.env.DATABASE_URL
if (!connectionString) {
  console.error('No se ha definido DATABASE_URL!')
  process.exit(1)
}
const pool = new Pool({ connectionString })

module.exports = pool
