create table users (
  id serial primary key,
  name text not null,
  birthday timestamp not null,
  title text not null,
  company text not null,
  bio text not null,
  avatar text not null
);
